@echo off 

color 0a
mode  60,30

:inicio 

cls

echo.

echo =======================================================
echo 	   			PACOTE OFFICE
echo =======================================================

echo.

echo [W] Word
ECHO.
echo [E] Excel
ECHO.
echo [A] Access
ECHO.
ECHO [P] Power Point
ECHO.
echo [R] Retornar ao Menu
ECHO.

echo ========================================================


echo.

set /p op=Digte uma opcao:

if %op% == w (goto:word)
if %op% == e (goto:excel)
if %op% == a (goto:access)
if %op% == p (goto:powerp)
if %op% == r (call menuprincipal.bat) else (
	echo.
	echo  Opcao invalida!
	pause
	goto inicio )
	
:word
	start winword.exe
	goto inicio
	
:excel 
	start excel.exe
	goto inicio
	
:powerp
	start powerpnt.exe
	goto inicio

:access
	start msaccess.exe
	goto inicio