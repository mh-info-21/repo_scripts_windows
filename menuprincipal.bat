@echo off 

color 0a
mode  60,30

:inicio 

cls

echo.

echo =======================================================
echo 	   			MENU PRINCIPAL
echo =======================================================

echo.

echo [1] Pacote Office
ECHO.
echo [2] Aplicativos diversos - Windows
ECHO.
echo [3] Servicos de Rede
ECHO.
ECHO [4] Gerenciamento da Maquina
ECHO.
echo [5] Encerrar Sessao
ECHO.
ECHO [6] Finalizar Programa

echo ========================================================


echo.

set /p op=Digte uma opcao:

if %op% == 1 (call office.bat)
if %op% == 2 (call diversos.bat)
if %op% == 3 (call rede.bat)
if %op% == 4 (call gerenciamento.bat)
if %op% == 5 (call login.bat)
if %op% == 6 (exit) else (
		echo.
		echo Opcao Invalida!
		pause
		goto inicio)
		


