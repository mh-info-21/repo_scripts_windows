@echo off 

color 0a
mode  60,30

:inicio 

cls

echo.

echo =======================================================
echo 	   			Gerenciamento da Maquina 
echo =======================================================

echo.

echo [d] Desligar a máquina
ECHO.
echo [r] Reinciar a máquina
ECHO.
echo [h] Hibernar a máquina
ECHO.
ECHO [l] Logoff
echo.
echo [r] Retornar ao Menu
ECHO.
echo ========================================================


echo.

set /p op=Digte uma opcao:

if %op% == d (goto:desligar)
if %op% == r (goto:reiniciar)
if %op% == h (goto:hibernar)
if %op% == l (goto:logoff)

if %op% == r (call menuprincipal.bat) else (
	echo.
	echo  Opcao invalida!
	pause
	goto inicio )
	
:desligar
	set /p tempo=Digite o tempo em que vai desligar:
	
	
	shutdown.exe -s -t %tempo%
	
:reiniciar 
	set /p tempo=Digite o tempo em que vai reinciar:
	shutdown.exe -r -t %tempo%
	
:hibernar
	powercfg off 

:logoff
	shutdown -i
	
