@echo off 

color 0a
mode  60,30

:inicio 

cls

echo.

echo =======================================================
echo 	   			Serviços de Rede
echo =======================================================

echo.

echo [i] Internet
ECHO.
echo [t] Ping IP
ECHO.
echo [o] Meu endereco de IP
ECHO.
echo [R] Retornar ao Menu
ECHO.

echo ========================================================


echo.

set /p op=Digte uma opcao:

if %op% == i (goto:navegador)
if %op% == t (goto:pingar)
if %op% == o (goto:meuip)
if %op% == r (call menuprincipal.bat) else (
	echo.
	echo  Opcao invalida!
	pause
	goto inicio )
	
:navegador
	set /p site=Digite a pagina web:
	start %site%
	goto inicio
	
:pingar
	set /p ping=Digite o IP da maquina ou endereço da pagina:
	ping %ping% -t
	goto inicio
	
:meuip
	ipconfig
	pause
