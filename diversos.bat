@echo off 

color 0a
mode  60,30

:inicio 

cls

echo.

echo =======================================================
echo 	   			Aplicativos Diversos Windows
echo =======================================================

echo.

echo [c] Calculadora
ECHO.
echo [b] Bloco de Notas
ECHO.
echo [t] Teclado Virtual
ECHO.
ECHO [p] Paint
ECHO.
echo [pc] Painel de Controle
ECHO.
echo [w] Windows Explorer
echo.
echo [r] Retornar ao Menu
ECHO.
echo ========================================================


echo.

set /p op=Digte uma opcao:

if %op% == c (goto:calc)
if %op% == b (goto:bloco)
if %op% == t (goto:teclado)
if %op% == p (goto:paint)
if %op% == pc (goto:painel)
if %op% == w (goto:winex)
if %op% == r (call menuprincipal.bat) else (
	echo.
	echo  Opcao invalida!
	pause
	goto inicio )
	
:calc
	start calc.exe
	goto inicio
	
:bloco 
	start notepad.exe
	goto inicio
	
:teclado
	start osk.exe
	goto inicio

:paint
	start mspaint.exe
	goto
	inicio
	
:winex
	start explorer.exe
	goto inicio
	
:painel 
	start control.exe
	goto
	inicio